"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
/**
 * This class is a JS wrapper for the public R6Stats API.
 * @see https://r6stats.com
 * @version 1.0
 * @author KevSlashNull
 */
class R6StatsAPI {
    /**
     * Initialize a R6StatsAPI instance.
     * @param key The API key required to access the API
     */
    constructor(key) {
        this.key = key;
    }
    /**
     * Get general statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    getGenericStats(username, platform) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getStats(username, platform, 'generic');
        });
    }
    /**
     * Get seasonal statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    getSeasonalStats(username, platform) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getStats(username, platform, 'seasonal');
        });
    }
    /**
     * Get operator statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    getOperatorStats(username, platform) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getStats(username, platform, 'operators');
        });
    }
    /**
     * Get weapon statistics by category for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    getWeaponCategoryStats(username, platform) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getStats(username, platform, 'weapon-categories');
        });
    }
    /**
     * Get weapon statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    getWeaponStats(username, platform) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getStats(username, platform, 'weapons');
        });
    }
    /**
     * Get the R6Stats leaderboard.
     * @param platform The platform of the game
     * @param region The region of the leaderboard, default is `all`
     * @param page The page of the leaderboard (limited to 50!)
     */
    getLeaderboard(platform, region = 'all', page = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield node_fetch_1.default(`https://api2.r6stats.com/public-api/leaderboard/${platform}/${region}?page=${page ||
                0}`, {
                headers: {
                    authorization: 'Bearer ' + this.key,
                },
            })).json();
        });
    }
    /**
     * Get game stats about a specific player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     * @param type The type of stats the request
     */
    _getStats(username, platform, type) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield node_fetch_1.default(`https://api2.r6stats.com/public-api/stats/${username}/${platform}/${type}`, {
                headers: {
                    authorization: 'Bearer ' + this.key,
                },
            })).json();
        });
    }
}
exports.default = R6StatsAPI;
