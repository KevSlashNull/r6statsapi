import fetch from 'node-fetch';
import { GenericStats } from './stats/GenericStats';
import { SeasonalStats } from './stats/SeasonalStats';
import { OperatorStats } from './stats/OperatorStats';
import { WeaponCatStats } from './stats/WeaponCatStats';
import { WeaponStats } from './stats/WeaponStats';
import { LeaderboardEntry } from './stats/Leaderboard';

type UbiPlatform = 'pc' | 'ps4' | 'xbox';
type UbiRegion = 'all' | 'ncsa' | 'emea' | 'apac';
type StatsType = 'generic' | 'seasonal' | 'operators' | 'weapon-categories' | 'weapons';

/**
 * This class is a JS wrapper for the public R6Stats API.
 * @see https://r6stats.com
 * @version 1.0
 * @author KevSlashNull
 */
export default class R6StatsAPI {
    private key: String;

    /**
     * Initialize a R6StatsAPI instance.
     * @param key The API key required to access the API
     */
    constructor(key: String) {
        this.key = key;
    }

    /**
     * Get general statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    async getGenericStats(username: String, platform: UbiPlatform): Promise<GenericStats> {
        return this._getStats(username, platform, 'generic');
    }

    /**
     * Get seasonal statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */

    async getSeasonalStats(username: String, platform: UbiPlatform): Promise<SeasonalStats> {
        return this._getStats(username, platform, 'seasonal');
    }

    /**
     * Get operator statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    async getOperatorStats(username: String, platform: UbiPlatform): Promise<OperatorStats> {
        return this._getStats(username, platform, 'operators');
    }

    /**
     * Get weapon statistics by category for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    async getWeaponCategoryStats(username: String, platform: UbiPlatform): Promise<WeaponCatStats> {
        return this._getStats(username, platform, 'weapon-categories');
    }

    /**
     * Get weapon statistics for a player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     */
    async getWeaponStats(username: String, platform: UbiPlatform): Promise<WeaponStats> {
        return this._getStats(username, platform, 'weapons');
    }

    /**
     * Get the R6Stats leaderboard.
     * @param platform The platform of the game
     * @param region The region of the leaderboard, default is `all`
     * @param page The page of the leaderboard (limited to 50!)
     */
    async getLeaderboard(
        platform: UbiPlatform,
        region: UbiRegion = 'all',
        page: Number = 0
    ): Promise<LeaderboardEntry[]> {
        return (await fetch(
            `https://api2.r6stats.com/public-api/leaderboard/${platform}/${region}?page=${page ||
                0}`,
            {
                headers: {
                    authorization: 'Bearer ' + this.key,
                },
            }
        )).json();
    }

    /**
     * Get game stats about a specific player.
     * @param username The username of the player
     * @param platform The platform the player plays the game on
     * @param type The type of stats the request
     */
    async _getStats(username: String, platform: UbiPlatform, type: StatsType): Promise<any> {
        return (await fetch(
            `https://api2.r6stats.com/public-api/stats/${username}/${platform}/${type}`,
            {
                headers: {
                    authorization: 'Bearer ' + this.key,
                },
            }
        )).json();
    }
}
