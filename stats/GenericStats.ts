export interface GenericStats {
    username: string;
    platform: string;
    ubisoft_id: string;
    uplay_id: string;
    avatar_url_146: string;
    avatar_url_256: string;
    last_updated: string;
    aliases: Alias[];
    progression: Progression;
    stats: Stats;
}

interface Stats {
    general: General;
    queue: Queue;
    gamemode: Gamemode;
    timestamps: Timestamps;
}

interface Timestamps {
    created: string;
    last_updated: string;
}

interface Gamemode {
    bomb: Bomb;
    secure_area: Securearea;
    hostage: Hostage;
}

interface Hostage {
    best_score: number;
    games_played: number;
    losses: number;
    playtime: number;
    extractions_denied: number;
    wins: number;
    wl: number;
}

interface Securearea {
    best_score: number;
    games_played: number;
    kills_as_attacker_in_objective: number;
    kills_as_defender_in_objective: number;
    losses: number;
    playtime: number;
    times_objective_secured: number;
    wins: number;
    wl: number;
}

interface Bomb {
    best_score: number;
    games_played: number;
    losses: number;
    playtime: number;
    wins: number;
    wl: number;
}

interface Queue {
    casual: QueueStats;
    ranked: QueueStats;
    other: QueueStats;
}

interface QueueStats {
    deaths: number;
    draws: number;
    games_played: number;
    kd: number;
    kills: number;
    losses: number;
    playtime: number;
    wins: number;
    wl: number;
}

interface General {
    assists: number;
    barricades_deployed: number;
    blind_kills: number;
    bullets_fired: number;
    bullets_hit: number;
    dbnos: number;
    deaths: number;
    distance_travelled: number;
    draws: number;
    gadgets_destroyed: number;
    games_played: number;
    headshots: number;
    kd: number;
    kills: number;
    losses: number;
    melee_kills: number;
    penetration_kills: number;
    playtime: number;
    rappel_breaches: number;
    reinforcements_deployed: number;
    revives: number;
    suicides: number;
    wins: number;
    wl: number;
}

interface Progression {
    level: number;
    lootbox_probability: number;
    total_xp: number;
}

interface Alias {
    username: string;
    last_seen_at: string;
}
