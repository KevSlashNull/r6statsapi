export interface SeasonalStats {
    username: string;
    platform: string;
    ubisoft_id: string;
    uplay_id: string;
    avatar_url_146: string;
    avatar_url_256: string;
    last_updated: string;
    seasons: Seasons;
}

interface Seasons {
    burnt_horizon: Season;
    wind_bastion: Season;
    grim_sky: Season;
    para_bellum: Season;
    chimera: Season;
    white_noise: Season;
    blood_orchid: Season;
    health: Season;
}

interface Season {
    name: string;
    start_date: string;
    end_date?: any;
    regions: Regions;
}

interface Regions {
    ncsa: Region[];
    emea: Region[];
    apac: Region[];
}

interface Region {
    region: string;
    abandons: number;
    losses: number;
    max_mmr: number;
    max_rank: number;
    mmr: number;
    next_rank_mmr: number;
    prev_rank_mmr: number;
    rank: number;
    skill_mean: number;
    skill_standard_deviation: number;
    created_for_date: string;
    wins: number;
    rank_text: string;
    rank_image: string;
}
