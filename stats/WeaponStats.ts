export interface WeaponStats {
    username: string;
    platform: string;
    ubisoft_id: string;
    uplay_id: string;
    avatar_url_146: string;
    avatar_url_256: string;
    last_updated: string;
    weapons: Weapon[];
}

export interface Weapon {
    weapon: string;
    category: string;
    kills: number;
    deaths: number;
    kd: number;
    headshots: number;
    headshot_percentage: number;
    times_chosen: number;
    bullets_fired: number;
    bullets_hit: number;
    created: string;
    last_updated: string;
}
